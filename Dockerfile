FROM jcdecaux/rhel:7
MAINTAINER Nicolas Mallet <nicolas.mallet@jcdecaux.com>

COPY MariaDB.repo /etc/yum.repos.d/MariaDB.repo

COPY yum.conf /etc/yum.conf

RUN yum install supervisor MariaDB-server MariaDB-client -y \
  && yum clean all

COPY supervisord.conf /etc/supervisord.conf

VOLUME ["/var/lib/mysql"]

EXPOSE 3306

CMD ["/usr/bin/supervisord"]
